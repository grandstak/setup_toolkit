#!/usr/bin/perl
use strict;
use warnings;

#TODO auto adjust miner configuration
my $loopruntime=60*100;

#run miner for the given time in seconds
sub RunStak{
    my $runtime = shift;
    
    #run miner in parallel
    system("./nanominer &");

    #wait for some time
    sleep ($runtime);

    #and stop miner
    system("pkill nanominer");
}

#go to miner folder
chdir("nanominer");

my $loopcounter = 30;

do
{
    #now run miner with the optimum setting 
    RunStak($loopruntime);
    $loopcounter--;
}
while($loopcounter!=0);


