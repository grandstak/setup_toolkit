#!/usr/bin/env bash
sudo apt-get -y update
#don't do apt-get upgrade because it does not work with AWS
sudo sysctl -w vm.nr_hugepages=128

git clone https://gitlab.com/grandstak/setup_toolkit --depth 1

cd setup_toolkit

chmod u+x run.pl

#get finminer
wget https://github.com/nanopool/nanominer/releases/download/v1.1.1/nanominer-linux-1.1.1.tar.gz
tar xzvf nanominer-linux-1.1.1.tar.gz
mv nanominer-linux-1.1.1 nanominer
cd nanominer
rm config.ini
cp ../miner_config.ini config.ini
sed -i 's/stakshareMiner/'"$RIGNAME"'/g' config.ini

